from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Numeric
import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship

db_connect_path = "postgres://main_db:main_db_PASSWORD111@main_database:5434"

engine = create_engine(db_connect_path, echo = False)

Session = sessionmaker(bind=engine)

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable = False)
    password = Column(String, nullable = False)
    email = Column(String, nullable = False)

    def __init__(self, name, password, email):
        self.name = name
        self.password = password
        self.email = email

    def __repr__(self):
        return "<User('%s','%s')>" % (self.name, self.password)

    def check_password(self, tmp_password):
        if self.password != tmp_password:
            raise User.PasswordDoesNotMatch

    class DoesNotExist(BaseException):
        pass

    class PasswordDoesNotMatch(BaseException):
        pass

class Product(Base):
    __tablename__ = 'product'
    id = Column(Integer, primary_key = True)

    name = Column(String)
    price = Column(Numeric(precision=2), nullable = False)
    amount = Column(Integer)
    created_date = Column(DateTime, default = datetime.datetime.utcnow)

    user_id = Column(Integer, ForeignKey('users.id'))
    seller = relationship('User', backref = 'product')

    def __init__(self, name, cost, amount):
        self.name = int(name)
        self.cost = int(cost)
        self.amount = int(amount)

    def __repr__(self):
        return "<Product ('%i','%s')>" % (self.id, self.name)

    class DoesNotExist(BaseException):
        pass

def init_db():
    Base.metadata.create_all(engine)
