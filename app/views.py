from datetime import datetime, timedelta
import app
import models
import middlewares
import jwt
from sqlalchemy import desc
from sqlalchemy.orm import sessionmaker
import ccxt
import handlers

async def parse_get_data(path):
    get_part = path.split('?')[1]
    get_data = dict()
    for item in get_part.split('&'):
        data = item.split('=')
        get_data.update({data[0]:data[1]})
    return get_data

def login_required(func):
    def wrapper(request):
        if not request.user:
            return app.json_response({'message': 'Auth required'}, status=401)
        return func(request)
    return wrapper

async def login(request):
    post_data = await request.post()
    print(post_data)
    try:
        user = models.User.get(post_data['username'])
        user.check_password(post_data['password'])
    except (models.User.DoesNotExist, models.User.PasswordDoesNotMatch):
        return app.json_response({'message': 'Wrong credentials'}, status = 400)

    payload = {
        'user_id': user.id,
        'exp': datetime.utcnow() + timedelta(seconds = middlewares.EXP_DELTA_SECONDS)
    }
    token = jwt.encode(payload, middlewares.SECRET, middlewares.ALGORITHM)
    return app.json_response({'token': token.decode('utf-8')}, status = 200)

async def register(request):
    post_data = await request.post()

    try:
        user = models.User.get(post_data['username'])
        if user:
            return app.json_response({'message': 'User same name is exist!'}, status = 400)
        user = models.User(post_data['username'],
            post_data['password'],
            post_data['email'])
        user.sub_type = [models.SubscribeType(0)]
        session = models.Session()
        session.add(user)
        session.commit()
    except Exception as e:
        return app.json_response({'message': str(e)}, status = 400)

    payload = {
        'user_id': user.id,
        'exp': datetime.utcnow() + timedelta(seconds = middlewares.EXP_DELTA_SECONDS)
    }
    token = jwt.encode(payload, middlewares.SECRET, middlewares.ALGORITHM)
    return app.json_response({'token': token.decode('utf-8')}, status = 200)


@login_required
async def add_product(request):
    post_data = await request.json()

    try:
        product = models.Product(
            post_data['name'],
            post_data['cost'],
            post_data['amount'])
        session = models.Session()
        session.add(product)
        session.commit()
    except Exception as e:
        return app.json_response({'message': str(e)}, status = 400)

    return app.json_response({'message': 'You add product'}, status = 200)

async def get_products(request):
    get_data = await parse_get_data(request.path_qs)

    try:
        products = handlers.filter_products(get_data).all()                                # Product.query.all()

        return app.json_response({'products': [{
                'name': item.name,
                'seller': item.seller.name,
                'cost': item.cost,
                'amount': item.amount,
                'created_date': item.created_date,
            } for item in products]}, status = 200)

    except Exception as e:
        return app.json_response({'message': str(e)}, status = 400)


async def stat_products(request):
    post_data = await request.json()

    try:
        products = handlers.filter_products(post_data).order_by(desc(Product.created_date)).all()
        formatted_response = dict()
        for item in products:
            if '{}/{}/{}'.format(item.created_date.day, item.created_date.month, item.created_date.year) in formatted_response:
                formatted_response[item.created_date]['products'] += 1
                formatted_response[item.created_date]['full_cost'] += item.price
                formatted_response[item.created_date]['sellers'].append(item.seller.name)
            else:
                formatted_response.update({
                    '{}/{}/{}'.format(item.created_date.day, item.created_date.month, item.created_date.year): {
                        'products': 1,
                        'full_cost': item.price,
                        'sellers': [item.seller.name, ]
                    }
                })

        for item in formatted_response.keys():
            formatted_response[item]['sellers'] = len(tuple(formatted_response[item]['sellers'])) 

        return app.json_response({'parents': [item.parent_id for item in elements]}, status = 200)
    except Exception as e:
        return app.json_response({'message': str(e)}, status = 400)
